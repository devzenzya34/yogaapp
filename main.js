const main = document.querySelector("main");
//Tableau qui stocke tous les exercices.
const basicArray = [
    {pic: 0, min: 1},
    {pic: 1, min: 1},
    {pic: 2, min: 1},
    {pic: 3, min: 1},
    {pic: 4, min: 1},
    {pic: 5, min: 1},
    {pic: 6, min: 1},
    {pic: 7, min: 1},
    {pic: 8, min: 1},
    {pic: 9, min: 1},
]

let exerciceArray = [];

//Initialise le exerciceArray et le stocke localStorage(appel fonction anonyme, se lance seul au démarrage de la page)
(() => {
    if (localStorage.exercices) {
        exerciceArray = JSON.parse(localStorage.exercices);
    } else {
        exerciceArray = basicArray;
    }
})();

//Class générateur d'exercice - traite une carte exercice
class Exercice {
    constructor() {
        this.index = 0;
        this.minutes = exerciceArray[this.index].min;
        this.seconds = 0;
    }

    updateCountdown() {
        this.seconds = this.seconds < 10 ? "0" + this.seconds : this.seconds;

        //traitement du compte à rebours
        setTimeout(() => {
            if (this.minutes === 0 && this.seconds === "00") {
                this.index++;
                this.ring();
                //Passage à l'exercice suivante
                if (this.index < exerciceArray.length) {
                    this.minutes = exerciceArray[this.index].min;
                    this.seconds = 0;
                    this.updateCountdown();
                } else {
                    return page.finish();
                }
            } else if (this.seconds === "00") {
                this.minutes--;
                this.seconds = 59;
                this.updateCountdown();
            } else {
                this.seconds--;
                this.updateCountdown();
            }
        }, 10);

        //Affichage
        return (main.innerHTML = `
            <div class="exercice-container">
            <p>${this.minutes}:${this.seconds}</p>
            <img src="./img/${exerciceArray[this.index].pic}.png" />
            <div>${this.index + 1}/${exerciceArray.length}</div>
            </div>`)
    }

    ring() {
        const audio = new Audio();
        audio.src = "ring.mp3";
        audio.play();

    }
}

//Objet regroupant toutes les fonctions utiles
const utils = {

    //Fonction gérant le contenu d'une page
    pageContent: function (title, content, btn) {
        document.querySelector("h1").innerHTML = title;
        main.innerHTML = content;
        document.querySelector(".btn-container").innerHTML = btn;
    },

    //fonction gérant le timing de l'exercices
    handleEventMinutes: function () {
        document.querySelectorAll('input[type="number"]').forEach((input) => {
            input.addEventListener("input", (e) => {
                //trouve l'id de l'exo (pic)
                exerciceArray.map((exo) => {
                    if (exo.pic == e.target.id) {
                        exo.min = parseInt(e.target.value);
                        //console.log(exerciceArray)
                        this.store();
                    }
                });
            })
        })
    },

    //gère l'évènement mofidier l'ordre des exercices
    handleEventArrow: function () {
        document.querySelectorAll(".arrow").forEach((arrow) => {
            arrow.addEventListener("click", (e) => {
                let position = 0;
                //trouve l'id de l'exo (pic) sur lequel on a clicker
                exerciceArray.map((exo) => {
                    if (exo.pic == e.target.dataset.pic && position !== 0) {
                        //inversion de la position de 2 objets dans un tableau en destructuring
                        [exerciceArray[position], exerciceArray[position - 1]] = [exerciceArray[position - 1], exerciceArray[position],];
                        page.home();
                        this.store();
                    } else {
                        position++;
                    }
                });
            });
        });
    },

    //suppression d'une certe exercice
    deleteItem: function () {
        document.querySelectorAll(".deleteBtn").forEach((btn) => {
            btn.addEventListener("click", (e) => {
                let newArr = [];
                exerciceArray.map((exo) => {
                    if (exo.pic != e.target.dataset.pic) {
                        newArr.push(exo);
                    }
                });
                exerciceArray = newArr;
                page.home();
                this.store();
            });
        });
    },

    //recommencer les exos à l'état initial
    reboot: function () {
        exerciceArray = basicArray;
        page.home();
        this.store();
    },
    // permet de stocker dans le local storage et sauvegardé les changements aux
    store: function () {
        localStorage.exercices = JSON.stringify(exerciceArray);
    },

}

//Objet gérant les pages
const page = {
    //page 1
    home: function () {
        //Une carte = exercice
        let mapArray = exerciceArray
            .map((exo) =>
                `
                    <li>
                    <div class="card-header">
                        <input type="number" id=${exo.pic} min="1" max="10" value=${exo.min}>
                        <span>min</span>
                    </div>
                    <img src="./img/${exo.pic}.png" />
                    <i class="fas fa-arrow-alt-circle-left arrow" data-pic=${exo.pic}></i>
                    <i class="fas fa-times-circle deleteBtn" data-pic=${exo.pic}></i>
                    </li>
                `
            )
            .join("");
        //affichage du contenu
        utils.pageContent(
            "Paramétrage <i id='reboot' class='fas fa-undo'></i>",
            "<ul>" + mapArray + "</ul>",
            "<button id='start'>Commencer<i class='far fa-play-circle'></i></button>"
        )

        //gestion du timing
        utils.handleEventMinutes();
        //gestion de l'ordre des exos
        utils.handleEventArrow();
        utils.deleteItem();
        // Attach event listener to bouton id=reboot ( pour checkbox, i , on a pas besoin de déclarer)
        reboot.addEventListener("click", () => utils.reboot());
        start.addEventListener("click", () => this.routine()); //bouton commencer
    },

    //page2
    routine: function () {
        const exercice = new Exercice();

        utils.pageContent(
            "Routine",
            exercice.updateCountdown(),
            null
        )
    },

    //page3,
    finish: function () {
        utils.pageContent(
            "C'est terminé !",
            "<button id='start'>Recommencer</button>",
            "<button id='reboot' class='btn-reboot'>Réinitialiser <i class='fas fa-times-circle'></i></button>"
        );
        start.addEventListener("click", () => this.routine());
        reboot.addEventListener("click", () => utils.reboot());
    }
}

page.home();